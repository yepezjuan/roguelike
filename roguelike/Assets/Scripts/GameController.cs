﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;
    public AudioClip gamestartsound;
    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject startScreen;
    private GameObject levelImage;
    private Text levelText;
    private Text startText;
    private bool settingUpGame;
    private int secondsUntilImageStart = 2;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;

    void Awake() {

        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
    }

    void Start()
    {
        InitilaizeStartScreen();
    }

    private void InitilaizeStartScreen()
    {
        startScreen = GameObject.Find("Start Screen");
        startText = GameObject.Find("Start Text").GetComponent<Text>();
        startText.text = "Welcome";
        Invoke("DisableStartScreen", secondsUntilImageStart);
    }

    private void InitializeGame ()
    {
        if(startScreen != null)
        {
            startScreen.SetActive(false);
        }

        else
        {
            startScreen = GameObject.Find("Start Screen");
            startScreen.SetActive(false);
        }

        settingUpGame = true;
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
    }

    private void  DisableStartScreen()
    {
        startScreen.SetActive(false);
        InitializeGame();
    }

    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }


    void Update()
    {
        if (isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
    } 


        private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach (Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }


        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void addEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }
    private bool CheckHighscore()
    {
        if (currentLevel > PlayerPrefs.GetInt("highscore", 0))
        {
            return true;
        }
        else return false;

    }

    private int GetHighscore()

    {
        if ( CheckHighscore())
        {
            PlayerPrefs.SetInt("highscore", currentLevel);
        }
        return PlayerPrefs.GetInt("highscore", 0);
    }



    public void GameOver()
    {
        bool brokeHighscore = CheckHighscore();
        int highscore = GetHighscore();
        isPlayerTurn = false;
        soundController.Instance.music.Stop();
        soundController.Instance.PlaySingle(gameOverSound);
        if(brokeHighscore)
        {
            levelText.text = "you starved after " + currentLevel + "days...\n" + "cool score: " + highscore;
        }

        else
        {  
            levelText.text = "you starved after " + currentLevel + "days...\n" + "Old Highscore: " + highscore;
        }

        
        levelImage.SetActive(true);
        enabled = false;
    }
}
